A NodeJS-Express-RabbitMQ examples to build a very simple message BUS ...



1) npm install

2) npm run build
  
3) nodemon ./build/app.js

4) nodemon build/src/app/workers/core-notificatios-worker.js

5) nodemon build/src/app/workers/cotizaciones-worker.js

6) nodemon build/src/app/workers/cotizaciones-worker.js

Once the above steps has been made, you can test the demo by hitting the
test endpoint like this using Postman or CURL:

POST http://localhost:3000/cotizaciones
BODY:
{
    "name": "JOHN DOE",
    "id": "V94100132",
    "address": "TAMPAL FL",
    "phone": "001-813-2359098",
    "email":"john.doe@gmail.com",
    "vehicles": [
        {
            "id" : "AD28TRY6",
            "brand" : "JEEP",
            "model" : "CHEROKEE",
            "year" : "2011",
            "service" : {
                "id": "2G",
                "duration": "12",
                "durationUnit": "MONTHS",
                "price": "50",
                "currency": "$"
            }
        },
        {
            "id" : "AG675IA",
            "brand" : "CHERVORLET",
            "model" : "AVEO",
            "year" : "2012",
            "service" : {
                "id": "3G",
                "duration": "12",
                "durationUnit": "MONTHS",
                "price": "65",
                "currency": "$"
            }
        } 
    ]
}

