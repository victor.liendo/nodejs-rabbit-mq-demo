"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MiscHelpers = void 0;
const models_1 = require("../models");
const app_config_1 = require("../../../config/app.config");
/**
 *  This class is intended to provide HELPERS functions for different purposes
 *
 */
class MiscHelpers {
    /**
     * setResponse: Sets up the generic EndpointResponse object to be used for all REST
     *              responses sent by ALL CONTROLLERS
     *
     * @param status: OK, ERROR or FATAL
     * @param code: the HTTP response code
     * @param data: a JSON object which can contain data
     * @param errors: an array of errors
     * @param message: a string containing a generic message about the result
     * @returns: and EndpointResponse object
    */
    static setResponse(status, code, data, errors, message) {
        const endpointResponse = new models_1.EndpointResponse();
        endpointResponse.setStatus(status);
        endpointResponse.setCode(code);
        if (data !== null) {
            endpointResponse.setData(data);
        }
        if (errors) {
            endpointResponse.setErrors(errors);
        }
        endpointResponse.setMessage(message);
        return endpointResponse;
    }
    static setDestinationQueue(message) {
        const jsonMessage = JSON.parse(message);
        let destQueue = '';
        switch (jsonMessage.type) {
            case 'COTIZACIONES': {
                switch (jsonMessage.subtype) {
                    case 'CREATE_PDF': {
                        destQueue = app_config_1.default.cotizaciones_queue;
                        break;
                    }
                    case 'PDF_CREATED': {
                        destQueue = app_config_1.default.emails_queue;
                        break;
                    }
                }
                break;
            }
        }
        return destQueue;
    }
}
exports.MiscHelpers = MiscHelpers;
