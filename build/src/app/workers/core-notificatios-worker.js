"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_config_1 = require("../../../config/app.config");
const amqp = require('amqplib/callback_api');
const misc_helpers_1 = require("../helpers/misc-helpers");
amqp.connect('amqp://localhost', function (connectionError, connection) {
    if (connectionError) {
        throw connectionError;
    }
    connection.createChannel(function (createChannelError, channel) {
        if (createChannelError) {
            throw createChannelError;
        }
        const sourceQueue = app_config_1.default.notifications_queue;
        // This makes sure the queue is declared before attempting to consume from it
        channel.assertQueue(sourceQueue, {
            durable: true
        });
        // we can use the prefetch method with the value of 1. This tells
        // RabbitMQ not to give more than one message to a consumer at a time.
        // Or, in other words, don't dispatch a new message to a consumer until
        // it has processed and acknowledged the previous one. Instead, it will
        // dispatch it to the next worker that is not still busy.
        channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", sourceQueue);
        // We'll simulate that the task which has to process the incoming messages
        // takes arond 10 secs to be completed ...
        channel.consume(sourceQueue, function (msg) {
            setTimeout(function () {
                //
                // 
                // Destination queue will be set depending on the info in the incoming message
                const destQueue = misc_helpers_1.MiscHelpers.setDestinationQueue(msg.content.toString());
                channel.assertQueue(destQueue, {
                    durable: true
                });
                channel.sendToQueue(destQueue, Buffer.from(msg.content.toString()), {
                    persistent: true
                });
                // Here we could store the message received by the core worker
                // in an DB structures like "events"
                console.log(" [x] Message from " + sourceQueue + " RECEIVED OK");
                console.log(" [x] Message  redirected to " + destQueue);
                channel.ack(msg);
            }, 5 * 1000);
        }, {
            // automatic acknowledgment mode,
            // see https://www.rabbitmq.com/confirms.html for details
            //noAck: true
            //
            //Lets turn on ACK, This way, if this consumer dies, connection is 
            // lost, etc, while doing the task, the message will be delivered
            // to another consumer ...
            noAck: false
        });
    });
});
