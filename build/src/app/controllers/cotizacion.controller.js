"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CotizacionController = void 0;
//import messages from '../../../config/app.messages';
const app_config_1 = require("../../../config/app.config");
const misc_helpers_1 = require("../helpers/misc-helpers");
const rabbitMQmessage_1 = require("../models/rabbitMQmessage");
const endpointResponse_1 = require("../models/endpointResponse");
class CotizacionController {
}
exports.CotizacionController = CotizacionController;
/*
  register: ...
*/
/* Express should be installed with @types/express */
CotizacionController.register = async (req, res) => {
    let endpointResponse = new endpointResponse_1.EndpointResponse();
    const amqp = require('amqplib/callback_api');
    let httpCode = 201;
    amqp.connect('amqp://localhost', function (connectionError, connection) {
        if (connectionError) {
            httpCode = 500;
            endpointResponse = misc_helpers_1.MiscHelpers.setResponse(endpointResponse_1.EndpointResponseStatus.FATAL, httpCode, null, [], connectionError);
            res.status(httpCode).send(endpointResponse);
        }
        connection.createChannel(function (createChannelError, channel) {
            if (createChannelError) {
                httpCode = 500;
                endpointResponse = misc_helpers_1.MiscHelpers.setResponse(endpointResponse_1.EndpointResponseStatus.FATAL, httpCode, null, [], createChannelError);
                res.status(500).send(endpointResponse);
            }
            const outputMessage = new rabbitMQmessage_1.RabbitMQMessage();
            outputMessage.setType('COTIZACIONES');
            outputMessage.setSubtype('CREATE_PDF');
            outputMessage.setData(req.body);
            const msg = JSON.stringify(outputMessage);
            console.log(msg);
            // GET THE QUEUE NAME FROM CONFIG ...
            const destQueue = app_config_1.default.notifications_queue;
            channel.assertQueue(destQueue, {
                durable: true
            });
            channel.sendToQueue(destQueue, Buffer.from(msg), {
                persistent: true
            });
            console.log("Cotización Data sent to " + destQueue + " :", msg);
        });
        setTimeout(function () {
            connection.close();
            endpointResponse = misc_helpers_1.MiscHelpers.setResponse(endpointResponse_1.EndpointResponseStatus.OK, 201, req.body, [], 'Cotización Message SENT Ok!!!');
            res.status(201).send(endpointResponse);
        }, 500);
    });
};
