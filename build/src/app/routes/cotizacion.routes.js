"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const controllers_1 = require("../controllers");
const router = express_1.Router();
// Get all Countries after checking if the Cognito Access Token is valid,
router.post('/', controllers_1.CotizacionController.register);
exports.default = router;
