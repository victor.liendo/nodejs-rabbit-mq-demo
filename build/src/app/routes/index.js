"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
// Importing routes from each routes file. I guess that
// the default object, variable or function exported by
// 'countries.routes.ts' file (called 'router' in this case, and only
// one object...) is assigned to the 'countryroutes' variable.
// Thus, it can have a different name in both the exporting
// and exporting file
const cotizacion_routes_1 = require("./cotizacion.routes");
const router = express_1.Router();
router.use('/cotizaciones', cotizacion_routes_1.default);
exports.default = router;
