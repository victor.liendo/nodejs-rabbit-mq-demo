"use strict";
// This class is intended to standarize the reponses for all the endpoints
Object.defineProperty(exports, "__esModule", { value: true });
exports.EndpointResponse = exports.EndpointResponseStatus = void 0;
var EndpointResponseStatus;
(function (EndpointResponseStatus) {
    EndpointResponseStatus["OK"] = "OK";
    EndpointResponseStatus["ERR"] = "ERROR";
    EndpointResponseStatus["FATAL"] = "FATAL";
})(EndpointResponseStatus = exports.EndpointResponseStatus || (exports.EndpointResponseStatus = {}));
class EndpointResponse {
    setStatus(status) {
        this.status = status;
    }
    setCode(code) {
        this.code = code;
    }
    setData(data) {
        this.data = data;
    }
    setErrors(error) {
        this.error = error;
    }
    setMessage(message) {
        this.message = message;
    }
    isOk() {
        if (this.status === EndpointResponseStatus.OK) {
            return true;
        }
        else {
            return false;
        }
    }
}
exports.EndpointResponse = EndpointResponse;
