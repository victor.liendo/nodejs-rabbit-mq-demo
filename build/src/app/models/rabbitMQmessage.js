"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RabbitMQMessage = void 0;
class RabbitMQMessage {
    setType(type) {
        this.type = type;
    }
    setSubtype(subtype) {
        this.subtype = subtype;
    }
    setData(data) {
        this.data = data;
    }
}
exports.RabbitMQMessage = RabbitMQMessage;
