"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EndpointResponse = exports.EndpointResponseStatus = void 0;
var endpointResponse_1 = require("./endpointResponse");
Object.defineProperty(exports, "EndpointResponseStatus", { enumerable: true, get: function () { return endpointResponse_1.EndpointResponseStatus; } });
var endpointResponse_2 = require("./endpointResponse");
Object.defineProperty(exports, "EndpointResponse", { enumerable: true, get: function () { return endpointResponse_2.EndpointResponse; } });
