"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const awsServerlessExpress = require('aws-serverless-express');
const app_1 = require("./app");
const server = awsServerlessExpress.createServer(app_1.default);
exports.handler = (event, context) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);
    awsServerlessExpress.proxy(server, event, context);
};
