

export class RabbitMQMessage
{
  type: string;
  subtype: string;
  data: object;
  
  setType (type: string): void {
    this.type = type;
  }

  setSubtype(subtype: string): void {
    this.subtype = subtype;
  }

  setData(data: object): void {
    this.data = data;
  }
}
