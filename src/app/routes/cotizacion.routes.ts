import { Router } from 'express';
import { CotizacionController } from '../controllers';

const router = Router();



// Get all Countries after checking if the Cognito Access Token is valid,
router.post('/', CotizacionController.register);

export default router;

