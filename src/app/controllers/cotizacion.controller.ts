import { Request, Response } from 'express';
//import messages from '../../../config/app.messages';
import config from '../../../config/app.config';
import { MiscHelpers } from '../helpers/misc-helpers';
import { RabbitMQMessage } from '../models/rabbitMQmessage'
import { EndpointResponseStatus, EndpointResponse } from '../models/endpointResponse';


export class CotizacionController {
  /*
    register: ...
  */
  /* Express should be installed with @types/express */ 
  static register = async (req: Request, res: Response)  => {
    let endpointResponse = new EndpointResponse();
    const amqp = require('amqplib/callback_api');
    let httpCode = 201;
    amqp.connect('amqp://localhost', function(connectionError, connection) {
      if (connectionError) {
          httpCode = 500;
          endpointResponse = MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, [], connectionError);
          res.status(httpCode).send(endpointResponse);
      }
      connection.createChannel(function(createChannelError, channel) {
          if (createChannelError) {
            httpCode = 500;
            endpointResponse = MiscHelpers.setResponse(EndpointResponseStatus.FATAL, httpCode, null, [], createChannelError);
            res.status(500).send(endpointResponse); 
          }
          const outputMessage = new RabbitMQMessage();
          outputMessage.setType('COTIZACIONES');
          outputMessage.setSubtype('CREATE_PDF')
          outputMessage.setData(req.body);
          const msg = JSON.stringify(outputMessage);
          console.log(msg);
          // GET THE QUEUE NAME FROM CONFIG ...
          const destQueue = config.notifications_queue;
          channel.assertQueue(destQueue, {
              durable: true
            });
          channel.sendToQueue(destQueue, Buffer.from(msg), {
              persistent: true
          });
          console.log("Cotización Data sent to " + destQueue + " :", msg);
      });
      setTimeout(function() {
          connection.close();
          endpointResponse = MiscHelpers.setResponse(EndpointResponseStatus.OK, 201, req.body, [], 'Cotización Message SENT Ok!!!');
          res.status(201).send(endpointResponse);
      }, 500);
    });   
  }
}
