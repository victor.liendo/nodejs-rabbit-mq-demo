import config from '../../../config/app.config';
const amqp = require('amqplib/callback_api');
//import { MiscHelpers } from '../helpers/misc-helpers';
//import { RabbitMQMessage } from '../models/rabbitMQmessage'

amqp.connect('amqp://localhost', function(connectionError, connection) {
    if (connectionError) {
        throw connectionError;
    }
    connection.createChannel(function(createChannelError, channel) {
        if (createChannelError) {
            throw createChannelError;
        }

        const sourceQueue = config.emails_queue;
        // This makes sure the queue is declared before attempting to consume from it
        channel.assertQueue(sourceQueue, {
          durable: true
        });
        // we can use the prefetch method with the value of 1. This tells
        // RabbitMQ not to give more than one message to a consumer at a time.
        // Or, in other words, don't dispatch a new message to a consumer until
        // it has processed and acknowledged the previous one. Instead, it will
        // dispatch it to the next worker that is not still busy.
        
        channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", sourceQueue);
        // We'll simulate that the task which has to process the incoming messages
        // takes arond 10 secs to be completed ...
        channel.consume(sourceQueue, function(msg) {
            // console.log(" [x] Received %s", msg.content.toString());
            setTimeout(function() {
                const jsonContent = JSON.parse(msg.content.toString());
                console.log(jsonContent);
                console.log(" [x] Message from " + sourceQueue + " PROCESSED OK");
                console.log(" [x] Cotización SENT by E-mail to : " + jsonContent.data.email) 
                channel.ack(msg);
            }, 10 * 1000);
        }, {
          // automatic acknowledgment mode,
          // see https://www.rabbitmq.com/confirms.html for details
          //noAck: true
          //
          //Lets turn on ACK, This way, if this consumer dies, connection is 
          // lost, etc, while doing the task, the message will be delivered
          // to another consumer ...
          noAck : false
        }); 
    });
});