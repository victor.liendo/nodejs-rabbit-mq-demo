import config from '../../../config/app.config';
const amqp = require('amqplib/callback_api');
const jsonfile = require('jsonfile')
const file = './PDF/cotizaciones.json'
import { MiscHelpers } from '../helpers/misc-helpers';
import { RabbitMQMessage } from '../models/rabbitMQmessage'

amqp.connect('amqp://localhost', function(connectionError, connection) {
    if (connectionError) {
        throw connectionError;
    }
    connection.createChannel(function(createChannelError, channel) {
        if (createChannelError) {
            throw createChannelError;
        }

        const sourceQueue = config.cotizaciones_queue;
        // This makes sure the queue is declared before attempting to consume from it
        channel.assertQueue(sourceQueue, {
          durable: true
        });
        // we can use the prefetch method with the value of 1. This tells
        // RabbitMQ not to give more than one message to a consumer at a time.
        // Or, in other words, don't dispatch a new message to a consumer until
        // it has processed and acknowledged the previous one. Instead, it will
        // dispatch it to the next worker that is not still busy.
        
        channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", sourceQueue);
        // We'll simulate that the task which has to process the incoming messages
        // takes arond 10 secs to be completed ...
        channel.consume(sourceQueue, function(msg) {
            // console.log(" [x] Received %s", msg.content.toString());
            setTimeout(function() {
                const todaysDate = new Date().toDateString().replace(/ /g, '-');;
                console.log(todaysDate);
                const destQueue = config.notifications_queue;
                //channel.assertQueue(destQueue, {
                //    durable: true
                //});
                //channel.sendToQueue(destQueue, Buffer.from(msg.content.toString()), {
                //    persistent: true
                //});
                const jsonContent = JSON.parse(msg.content.toString());
                //
                // We should convert to PDF. Instead, we will just write a JSON to the filesystem
                //
                const file = './PDF/cotizacion-' + jsonContent.data.email + '-' + todaysDate + '.json'
                jsonfile.writeFile(file, jsonContent.data, {spaces:2,  EOL: '\r\n' }, function(writeFileErr) {
                    if (writeFileErr) {
                       console.error(writeFileErr); 
                    } else {
                        const data = {
                            email: jsonContent.data.email,
                            subject: "Cotización",
                            attachments: [
                                {file: file}
                            ] 
                        }
                        const outputMessage = new RabbitMQMessage();
                        outputMessage.setType('COTIZACIONES');
                        outputMessage.setSubtype('PDF_CREATED')
                        outputMessage.setData(data);
                        const msg = JSON.stringify(outputMessage);
                        channel.assertQueue(destQueue, {
                            durable: true
                        });
                        channel.sendToQueue(destQueue, Buffer.from(msg), {
                            persistent: true
                        });
                        console.log(" [x] Message from " + sourceQueue + " PROCESSED OK");
                        console.log(" [x] new Message SENT to " + destQueue);
                    }    
                  })  
                channel.ack(msg);
            }, 10 * 1000);
        }, {
          // automatic acknowledgment mode,
          // see https://www.rabbitmq.com/confirms.html for details
          //noAck: true
          //
          //Lets turn on ACK, This way, if this consumer dies, connection is 
          // lost, etc, while doing the task, the message will be delivered
          // to another consumer ...
          noAck : false
        }); 
    });
});