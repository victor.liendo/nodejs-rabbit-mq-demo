import {
        EndpointResponse,
        EndpointResponseStatus,
       } from '../models';
import config from '../../../config/app.config';

/**
 *  This class is intended to provide HELPERS functions for different purposes
 *
 */

export abstract class  MiscHelpers {

    /**
     * setResponse: Sets up the generic EndpointResponse object to be used for all REST
     *              responses sent by ALL CONTROLLERS
     *
     * @param status: OK, ERROR or FATAL
     * @param code: the HTTP response code
     * @param data: a JSON object which can contain data
     * @param errors: an array of errors
     * @param message: a string containing a generic message about the result
     * @returns: and EndpointResponse object
    */

    
    static setResponse(status: EndpointResponseStatus, code: number, data: object, errors: object[], message: string): EndpointResponse {
        const endpointResponse = new EndpointResponse();
        endpointResponse.setStatus(status);
        endpointResponse.setCode(code);
        if ( data !== null ) {
            endpointResponse.setData(data);
        }
        if (errors) {
            endpointResponse.setErrors(errors);
        }
        endpointResponse.setMessage(message);
        return endpointResponse;
    }

    static setDestinationQueue (message: string) : string {
        const jsonMessage = JSON.parse(message);
        let destQueue = '';
        switch (jsonMessage.type) {
            case 'COTIZACIONES' : {
                switch (jsonMessage.subtype) {
                    case 'CREATE_PDF' : {
                        destQueue =  config.cotizaciones_queue;
                        break;        
                    }
                    case 'PDF_CREATED' : {
                        destQueue = config.emails_queue;
                        break;
                    }
                }
                break;
            }
        }
        return destQueue;
    }
}

